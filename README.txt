* How to run the project:

- Clone the repo
- On console go project folder and run: mvn package
- Download Jetty from: http://download.eclipse.org/jetty/ (tested with version 8)
- Unzip Jetty and put java-webapp-coding-test-0.0.1-SNAPSHOT.war into webapps Jetty folder
- Download H2 console tool from http://www.h2database.com/html/main.html and unzip it
- Run H2 from console with: java -jar bin/h2-1.4.187.jar (on H2 folder)
- Open browser and go: http://localhost:8082/
- Inform H2 driver class (org.h2.Driver), new database location (jdbc:h2:<project location>/database)
- On H2 console run the contents of database_creation.sql file
- Disconnect H2
- Create the database , and create the database (detalhar)
- Open a console, go Jetty folder and run it using: java -jar start.jar

The project will be available on http://localhost:8080/codingtest/
There are no user friendly servlets, just the services.


* How to run tests:

Normally it can be done in project folder just running "mvn test", but its not working (there are some problems in the classpath). So the steps above will do it inside Eclipse (which is working fine).

- Clone the repo
- On console go project folder and run: mvn eclipse:eclipse
- Start eclipse
- Import project with right click on project explorer/Import/Import Existing Project
- Download H2 console tool from http://www.h2database.com/html/main.html and unzip it
- Run H2 from console with: java -jar bin/h2-1.4.187.jar (on H2 folder)
- Open browser and go: http://localhost:8082/
- Inform H2 driver class (org.h2.Driver), new database location (jdbc:h2:<project location>/database)
- On H2 console run the contents of database_creation.sql file
- Disconnect H2
- Go classes com.avenuecode.resource.OrderResourceTests and com.avenuecode.resource.ProductResourceTests and run by menu.

All implemented tests are passing.


* Notes

The following methods was implemented, and are working:

Product: list catalog
Product: read a single product by its id
Orders: list placed orders
Orders: read an existing order by its id

The following method was not implemented:
Orders: place an order
Orders: modify and existing order


I had no time to finish these last two methods. I had big problems and spent much time with DBUnit (its was my first time with it) and had some few difficulties with H2, Jetty (first time on both) and Hibernate (normally I use annotations, I spent some time learning how to make mappings on XML). I also tried to setup a log framework with logback with no success.