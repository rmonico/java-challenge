package com.avenuecode.resource;

import java.io.FileInputStream;
import java.net.URI;
import java.sql.Connection;
import java.sql.DriverManager;

import javax.ws.rs.core.UriBuilder;

import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.eclipse.jetty.server.Server;
import org.glassfish.jersey.jetty.JettyHttpContainerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.junit.After;
import org.junit.Before;

public class ResourceTests {

    private static final String CONNECTION_STRING = "jdbc:h2:./database";
    private static final String DRIVER_CLASS = "org.h2.Driver";
    private Server server;

    private IDatabaseConnection getConnection() throws Exception {
        Class.forName(DRIVER_CLASS);
        Connection jdbcConnection = DriverManager.getConnection(CONNECTION_STRING);

        return new DatabaseConnection(jdbcConnection);
    }

    private IDataSet getDataSet() throws Exception {
        return new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/resources/dbunit_dataset.xml"));
    }

    @Before
    public void startDBUnit() throws Exception {
        IDatabaseConnection connection = getConnection();

        IDataSet dataSet = getDataSet();

        try {
            DatabaseOperation.CLEAN_INSERT.execute(connection, dataSet);
        } finally {
            connection.close();
        }
    }

    @Before
    public void startJettyServer() {
        URI baseUri = UriBuilder.fromUri("http://localhost/").port(8080).build();
        ResourceConfig config = new ResourceConfig(ProductResource.class, OrderResource.class);
        server = JettyHttpContainerFactory.createServer(baseUri, config);
    }

    @After
    public void stopJettyServer() throws Exception {
        server.stop();
    }

}
