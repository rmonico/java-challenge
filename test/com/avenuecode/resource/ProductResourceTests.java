package com.avenuecode.resource;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import org.junit.Test;

import com.avenuecode.model.Product;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.TypeFactory;

public class ProductResourceTests extends ResourceTests {

    @Test
    public void should_list_all_products() throws Exception {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080");
        String content = target.path("/product/list").request().get(String.class);

        ObjectMapper mapper = new ObjectMapper();

        TypeFactory t = TypeFactory.defaultInstance();

        CollectionType productListType = t.constructCollectionType(ArrayList.class, Product.class);

        List<Product> products = mapper.readValue(content, productListType);

        assertEquals(2, products.size());

        Product product = products.get(0);

        assertEquals(new Long(1), product.getId());
        assertEquals("Phillips Headphone SHP 2000", product.getName());

        product = products.get(1);

        assertEquals(new Long(2), product.getId());
        assertEquals("Avell Notebook Titanium B154", product.getName());
    }

    @Test
    public void should_list_one_product_by_id() throws JsonParseException, JsonMappingException, IOException {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080");
        String content = target.path("/product/get_by_id/1").request().get(String.class);

        ObjectMapper mapper = new ObjectMapper();

        Product product = mapper.readValue(content, Product.class);

        assertEquals(new Long(1), product.getId());
        assertEquals("Phillips Headphone SHP 2000", product.getName());
    }
}
