package com.avenuecode.resource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import org.junit.Test;

import com.avenuecode.model.Order;
import com.avenuecode.model.Product;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.TypeFactory;

public class OrderResourceTests extends ResourceTests {

    @Test
    public void should_list_all_orders() throws Exception {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080");
        String content = target.path("/order/list").request().get(String.class);

        ObjectMapper mapper = new ObjectMapper();

        TypeFactory t = TypeFactory.defaultInstance();

        CollectionType orderListType = t.constructCollectionType(ArrayList.class, Order.class);

        List<Order> orderList = mapper.readValue(content, orderListType);

        assertEquals(2, orderList.size());

        Order order = orderList.get(0);

        assertEquals(new Long(1), order.getId());
        assertEquals(49096, order.getNumber());
        Product[] products = order.getProducts().toArray(new Product[] {});
        assertEquals(2, products.length);

        assertTrue(isProductIDOnArray(1L, products));
        assertTrue(isProductIDOnArray(2L, products));

        order = orderList.get(1);

        assertEquals(new Long(2), order.getId());
        assertEquals(54486, order.getNumber());
        products = order.getProducts().toArray(new Product[] {});
        assertEquals(1, products.length);

        assertEquals(new Long(2), products[0].getId());
    }

    private boolean isProductIDOnArray(long productID, Product[] products) {
        for (Product product : products) {
            if (productID == product.getId()) {
                return true;
            }
        }

        return false;
    }

    @Test
    public void should_get_product_by_id() throws JsonParseException, JsonMappingException, IOException {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080");
        String content = target.path("/order/get_by_id/1").request().get(String.class);

        ObjectMapper mapper = new ObjectMapper();

        Order order = mapper.readValue(content, Order.class);

        assertEquals(new Long(1), order.getId());
        assertEquals(49096, order.getNumber());

        Product[] products = order.getProducts().toArray(new Product[] {});
        assertEquals(2, products.length);

        assertTrue(isProductIDOnArray(1L, products));
        assertTrue(isProductIDOnArray(2L, products));
    }
}
