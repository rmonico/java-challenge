package com.avenuecode.dao;

import com.avenuecode.model.Product;

public class ProductDAO extends DAO<Product> {

    @Override
    protected String getTableName() {
        return "Product";
    }

}
