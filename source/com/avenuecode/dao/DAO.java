package com.avenuecode.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import com.avenuecode.model.Product;

public abstract class DAO<T> {

    private static SessionFactory sessionFactory;

    private void createSessionFactory() {
        if (sessionFactory != null)
            return;

        Configuration configuration = new Configuration();

        configuration.configure("hibernate.cfg.xml");

        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();

        sessionFactory = configuration.buildSessionFactory(serviceRegistry);
    }

    private Session getSession() {
        createSessionFactory();

        Session session = sessionFactory.openSession();

        return session;
    }

    public List<T> listAll() {
        Session session = getSession();

        Query query = session.createQuery(String.format("from %s", getTableName()));

        List list = query.list();

        return list;
    }

    public T getById(long id) {
        Session session = getSession();

        Query query = session.createQuery(String.format("from %s where id = :id", getTableName()));

        query.setParameter("id", id);

        return (T) query.uniqueResult();
    }

    protected abstract String getTableName();

}
