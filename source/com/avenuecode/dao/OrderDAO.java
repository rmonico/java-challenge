package com.avenuecode.dao;

import com.avenuecode.model.Order;

public class OrderDAO extends DAO<Order> {

    @Override
    protected String getTableName() {
        return "Order";
    }

}
