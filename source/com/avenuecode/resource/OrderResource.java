package com.avenuecode.resource;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.avenuecode.dao.OrderDAO;
import com.avenuecode.model.Order;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Path("order")
public class OrderResource {

    @Path("list")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String list() throws JsonProcessingException {
        List<Order> orderList = new OrderDAO().listAll();

        ObjectMapper mapper = new ObjectMapper();

        return mapper.writeValueAsString(orderList);
    }

    @Path("get_by_id/{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getById(@PathParam("id") long id) throws JsonProcessingException {
        Order order = new OrderDAO().getById(id);

        ObjectMapper mapper = new ObjectMapper();

        return mapper.writeValueAsString(order);
    }
}
