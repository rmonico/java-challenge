package com.avenuecode.resource;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.avenuecode.dao.ProductDAO;
import com.avenuecode.model.Product;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Path("product")
public class ProductResource {

    @Path("list")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String list() throws JsonProcessingException {
        List<Product> productList = new ProductDAO().listAll();

        ObjectMapper mapper = new ObjectMapper();

        return mapper.writeValueAsString(productList);
    }

    @Path("get_by_id/{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getById(@PathParam("id") long id) throws JsonProcessingException {
        Product product = new ProductDAO().getById(id);

        ObjectMapper mapper = new ObjectMapper();

        return mapper.writeValueAsString(product);
    }
}
