package com.avenuecode.model;

import java.util.HashSet;
import java.util.Set;

public class Order {

    private Long id;
    private int number;
    private Set<Product> products = new HashSet<Product>(0);

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }

}
